# Important Non-Person Accounts

## Accounts w/ Admin Rights:

* admin-mgoogle@domain.umich.edu (super admin)
  > This is a generic admin account to be used for things that shouldn't be owned by a person-admin.
  > e.x. The shared account request form is owned and authorized by this account.
* admin-google-shim@domain.umich.edu (super admin)
  > This account owns the OAuth2 credentials used by the Consensus Google Shim that facilitates communication between MCommunity and Google.
*ITSGoogle-McWebApp01@domain.umich.edu (super admin)
  > This account owns the OAuth2 credentials currently used to initially create groups and set ACL's in Google after they're created in MCommunity.
  > This functionality is planned to be merged into the new shim in the future.

**_Suspending any of these accounts is (typically) a bad idea unless completely necessary, as critical data will stop syncing between systems if the accounts are unavailable. Check with senior staff before taking any action as such._**

## Accounts w/o Admin Rights:

* collaborate@umich.edu
  > Support collab forum and calendar of campus outreach/training/consulting
* collaborationservices@umich.edu
  > Account that owns files/folders used by the team
* collabsharedcreate@umich.edu
  > This is the account that created and authorized the shared account creation Google Script. Shared account creation request tickets that come into ServiceLink come in from this address.
* googlecrm@umich.edu
* googleteam-temp@umich.edu
* m-boxteam@umich.edu
* mgoogleteam@umich.edu
  > Used by mostly Rita for communications stuff. Also owns a bunch of documents.
* mgoogle.ids@umich.edu
